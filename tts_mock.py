import asyncio
import logging
import os
from aiohttp import web
from asyncio import sleep


TTS_PORT = int(os.getenv('TTS_PORT', 8088))
DELAY = 1

logger = logging.getLogger(__name__)


async def stream_data(transport, data=''):
    logger.info('request started')
    for c in data:
        await sleep(DELAY)
        transport.sendto(c.encode())
        logger.debug('pronounced %s', c)
    transport.sendto(b'\x00')
    logger.info('request complete')


async def stream_speech(loop, remote_addr, request):
    transport, _ = await loop.create_datagram_endpoint(
        lambda: asyncio.DatagramProtocol(),
        remote_addr=remote_addr
    )

    return await stream_data(transport, data=request)


async def get_speech(request):
    loop = request.app.loop
    query = await request.json()
    try:
        remote_addr = (query['host'], query['port'])
    except KeyError:
        raise web.HTTPBadRequest()

    response = web.HTTPNoContent()
    stream = asyncio.ensure_future(
        stream_speech(loop, remote_addr, query.get('text', '')),
        loop=loop
    )
    await response.prepare(request)
    await response.write_eof()
    await stream
    return response


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    logging.getLogger('aiohttp').setLevel(logging.WARNING)
    logger.info('TTS started')
    app = web.Application()
    app.router.add_post('/', get_speech)
    web.run_app(app, port=TTS_PORT)
    logger.info('TTS stopped')
