#!/usr/bin/env python
import logging
from sys import argv
from make_request import SpeakerRpcClient


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    logging.getLogger('pika').setLevel(logging.WARNING)
    speaker = SpeakerRpcClient()
    speaker.stop_speak(argv[1])
    speaker.wait()
