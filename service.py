import aiohttp
import asyncio
import json
import logging
import os
import pika
from functools import partial


CAPACITY = int(os.getenv('CAPACITY', 5000))
LICENSES = int(os.getenv('LICENSES', 10))
AMQP = os.getenv('AMQP', 'amqp://guest:guest@localhost:5672/')
TTS_URL = os.getenv('TTS_URL', 'http://localhost:8088/')
MY_HOST = os.getenv('MY_HOST', 'localhost')
FIRST_PORT = 8801

logger = logging.getLogger(__name__)


class UdpProxy(asyncio.DatagramProtocol):
    def __init__(self, target, complete_future):
        self.target = target
        self.future = complete_future
        self.transport = None
        self.message = bytearray()

    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, _):
        self.message += data
        self.target.sendto(data=data)
        if data == b'\x00':
            self.transport.close()
            if not self.future.cancelled():
                self.future.set_result(self.message)


def create_stream_proxy(target, complete_future):
    return UdpProxy(target, complete_future)


class SpeechPool:
    METHODS = dict()
    CACHE = dict()

    def __init__(self, amqp_parameters):
        self.connection = pika.adapters.AsyncioConnection(amqp_parameters, self.on_connection_open)
        self.start_speak_channel = None
        self.stop_speak_channel = None
        self.cancelled = set()
        self.running_tasks = dict()
        self.loop = self.connection.loop
        self.queue = asyncio.Queue(LICENSES, loop=self.loop)
        self.tts_session = None
        self.tts_ports = list(range(FIRST_PORT, FIRST_PORT + LICENSES))

    def run(self):
        logger.info('SpeechPool started')
        self.connection.ioloop.start()

    def stop(self):
        self.connection.close()
        logger.info('SpeechPool stopped')

    def on_connection_open(self, connection):
        connection.channel(self.on_start_speak_channel_open)
        connection.channel(self.on_stop_speak_channel_open)

    def on_start_speak_channel_open(self, channel):
        channel.basic_qos(prefetch_count=CAPACITY)
        self.start_speak_channel = channel
        channel.queue_declare(self.on_start_speak_queue, queue='start_speak')

    def on_stop_speak_channel_open(self, channel):
        channel.exchange_declare(self.on_stop_speak_exchange, 'stop_speak', exchange_type='fanout')
        channel.basic_qos(prefetch_count=1)
        self.stop_speak_channel = channel

    def on_stop_speak_exchange(self, _):
        self.stop_speak_channel.queue_declare(self.on_stop_speak_queue, queue='stop_speak')

    def on_start_speak_queue(self, _):
        logger.debug('start_speak queue on')
        self.start_speak_channel.basic_consume(self.on_start_speak, queue='start_speak')

    def on_stop_speak_queue(self, _):
        logger.debug('stop_speak queue on')
        self.stop_speak_channel.queue_bind(self.on_stop_speak_bind, 'stop_speak', 'stop_speak', '')

    def on_stop_speak_bind(self, _):
        self.stop_speak_channel.basic_consume(self.on_stop_speak, queue='stop_speak', no_ack=True)

    def on_start_speak(self, ch, method, props, body):
        request_id = props.correlation_id.encode('utf-8')
        logger.info('start_speak request id %s', request_id)

        def task_done(fut):
            self.running_tasks.pop(request_id, None)
            try:
                fut.result()
                status = 'ok'
            except asyncio.CancelledError:
                status = 'cancelled'
            except asyncio.QueueFull:
                logger.info('TTS license limit reached')
                status = 'limit reached'
            except Exception as ex:
                status = 'server error: ' + str(ex)
                logger.critical(status)
            self.signal_completed(ch, method, props, status)

        if request_id in self.cancelled:
            self.cancelled.remove(request_id)
            fut = asyncio.Future(loop=self.loop)
            fut.add_done_callback(task_done)
            fut.cancel()
        else:
            try:
                kwargs = json.loads(body.decode(encoding='utf-8'))
                coro = self.start_speak(**kwargs)
                fut = asyncio.ensure_future(coro, loop=self.loop)
                self.running_tasks[request_id] = fut
                fut.add_done_callback(task_done)
            except Exception as ex:
                reason = 'Bad call: {}'.format(ex)
                self.signal_completed(ch, method, props, reason)

    def signal_completed(self, ch, method, props, status):
        logger.info('request completed with status: %s', status)
        ch.basic_publish('', routing_key=props.reply_to, properties=props, body=status)
        ch.basic_ack(delivery_tag=method.delivery_tag)

    def on_stop_speak(self, ch, method, props, body):
        logger.info('stop_speak request id %s', body)
        if body in self.running_tasks:
            self.running_tasks[body].cancel()
        else:
            self.cancelled.add(body)

    async def start_speak(self, text, host, port):
        end_user, _ = await self.loop.create_datagram_endpoint(
            lambda: asyncio.DatagramProtocol(),
            remote_addr=(host, port)
        )

        if text in self.CACHE:
            logger.info('playback from cache')
            end_user.sendto(data=self.CACHE[text])
        else:
            self.queue.put_nowait(None)
            logger.info('request to TTS')
            self.tts_session = self.tts_session or aiohttp.ClientSession(loop=self.loop)

            def update_cache(future):
                if not future.cancelled() and not future.exception():
                    self.CACHE[text] = future.result()
                logger.info('TTS request completed')

            socket = (MY_HOST, self.tts_ports.pop())
            playback_done = asyncio.Future(loop=self.loop)
            playback_done.add_done_callback(update_cache)
            stream_proxy_factory = partial(UdpProxy, end_user, playback_done)
            await self.loop.create_datagram_endpoint(stream_proxy_factory, local_addr=socket)

            request_params = {'host': MY_HOST, 'port': socket[1], 'text': text}
            await self.tts_session.post(TTS_URL, json=request_params)
            await playback_done


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    logging.getLogger('pika').setLevel(logging.WARNING)
    service = SpeechPool(pika.URLParameters(AMQP))
    try:
        service.run()
    except KeyboardInterrupt:
        service.stop()
