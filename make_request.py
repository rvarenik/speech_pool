#!/usr/bin/env python
import json
import logging
import pika
import uuid
from sys import argv


class SpeakerRpcClient:
    def __init__(self):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
        self.channel = self.connection.channel()
        on_completed_queue = self.channel.queue_declare(exclusive=True)
        self.on_completed_queue = on_completed_queue.method.queue
        self.channel.basic_consume(self.on_completed, no_ack=True, queue=self.on_completed_queue)
        self.requests = set()

    def on_completed(self, ch, method, props, body):
        if props.correlation_id in self.requests:
            self.requests.remove(props.correlation_id)
            print('request completed with result: {}'.format(body.decode('utf-8')))

    def start_speak(self, text, host, port):
        request_id = str(uuid.uuid4())
        body = json.dumps({'text': text, 'host': host, 'port': port})
        props = pika.BasicProperties(
            reply_to=self.on_completed_queue,
            correlation_id=request_id,
            content_type='application/json'
        )
        self.channel.basic_publish('', 'start_speak', properties=props, body=body)
        self.requests.add(request_id)
        return request_id

    def stop_speak(self, request_id):
        self.channel.basic_publish('stop_speak', '', body=request_id)

    def wait(self):
        self.connection.process_data_events()


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    logging.getLogger('pika').setLevel(logging.WARNING)
    speaker = SpeakerRpcClient()
    text = argv[1] if len(argv) > 1 else 'Hello world!!!'
    request_id = speaker.start_speak(text, 'localhost', 42)
    print(request_id)
    speaker.wait()
