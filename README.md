# README #

* tts_mock.py - substitution of a TTS server with simulation of RTP via simple UDP protocol.
* make_request.py - sample client that makes start_speak call
* cancel_request - sample client that cancels requests
* service.py - the service

### Setup ###

Create `virtualenv` and activate it:
    virtualenv --python=3.5 .env
    source .env/scripts/activate (Linux) -or- .env\scripts\activate (Windows)
    pip install -r requirements.txt

Get a RabbitMQ instance (if necessary):

    docker run -d --hostname my-rabbit --name ra -p 4369:4369 -p 5671:5671 -p 5672:5672 -p 8080:15672 -p 25672:25672 rabbitmq:3-management

### Usage ###

start TTS mock:

    python tts_mock.py

start the service:

    python service.py

Now you can send requests for speaking via make_request.py:

    python make_request.py [<text to speak>]
    python make_request.py "Say that again"

You will receive id of the started task. You may leave it and let it complete or cancel with cancel_request.py:

    python cancel_request.py <request_id>
    python cancel_request.py 276ac989-7e19-4f8f-96bd-665b42fb219b

You may combine both to produce heat:

    python make_request.py "Don't speak" | xargs python cancel_request.py
